var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');
var AppActions = require('../src/js/actions/AppActions');

jest.dontMock('../src/js/components/Book.react');

describe('Book.react', function () {
  var Book = require('../src/js/components/Book.react');
  var book = {
    objectId: 'XXX',
    isbn: '123456789X',
    title: 'Book',
    year: 2000
  };

  it('renders the Book component correctly', function () {
    var renderer = TestUtils.createRenderer();
    var result;
    var children;

    renderer.render(<Book key={0} nr={1} book={book}></Book>);

    result = renderer.getRenderOutput();
    children = result.props.children;

    // Count number
    expect(children[0].type).toEqual('td');
    expect(children[0].props.children).toEqual(1);
    // isbn
    expect(children[1].type).toEqual('td');
    expect(children[1].props.children).toEqual('123456789X');
    // title
    expect(children[2].type).toEqual('td');
    expect(children[2].props.children).toEqual('Book');
    // year
    expect(children[3].type).toEqual('td');
    expect(children[3].props.children).toEqual(2000);
    // update button
    expect(children[4].props.children[0].props.to).toEqual('/update/XXX');
    expect(children[4].props.children[0].props.children).toEqual('Update');
    // delete button
    expect(children[4].props.children[1].props.children).toEqual('Delete');
    expect(children[4].props.children[1].props.onClick).toBeDefined();
  });

  it('calls `AppActions.deleteBook` with the right arguments when clicking on "delete" button', function () {
    var renderer = TestUtils.createRenderer();
    var result;
    var children;

    renderer.render(<Book key={0} nr={1} book={book}></Book>);

    result = renderer.getRenderOutput();
    children = result.props.children;

    // click on the "delete" button
    children[4].props.children[1].props.onClick();
    expect(AppActions.deleteBook).toBeCalledWith(book)
  })
});