var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  _createBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.createBook({
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value)
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var success = notifications.success;

    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label forHtml="isbn">ISBN</label>
          <input defaultValue="" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
          {errors.isbn && <span className="text-danger">{errors.isbn}</span>}
        </div>
        <div className="form-group">
          <label forHtml="title">Title</label>
          <input defaultValue="" ref="title" type="text" className="form-control" id="title" placeholder="Title" />
          {errors.title && <span className="text-danger">{errors.title}</span>}
        </div>
        <div className="form-group">
          <label forHtml="year">Year</label>
          <input defaultValue="" ref="year" type="text" className="form-control" id="year" placeholder="Year" />
          {errors.year && <span className="text-danger">{errors.year}</span>}
        </div>
        <button type="submit" onClick={this._createBook} className="btn btn-default">Submit</button>
        {success && <p className="bg-success">Success!</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});