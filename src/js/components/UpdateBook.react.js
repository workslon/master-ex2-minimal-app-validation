var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  componentWillMount: function () {
    this.book = AppStore.getBook(this.props.params.id);
  },

  _updateBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.updateBook(this.book, {
      title: title.value,
      year: parseInt(year.value)
    });
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var success = notifications.success;

    return (
        <div>
          <h3>Update Book</h3>
          {this.book ?
            <form>
              <div className="form-group">
                <label forHtml="isbn">ISBN</label>
                <input defaultValue={this.book.isbn} disabled="disabled" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
              </div>
              <div className="form-group">
                <label forHtml="title">Title</label>
                <input defaultValue={this.book.title} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
                {errors.title && <span className="text-danger">{errors.title}</span>}
              </div>
              <div className="form-group">
                <label forHtml="year">Year</label>
                <input defaultValue={this.book.year} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
                {errors.year && <span className="text-danger">{errors.year}</span>}
              </div>
              <button type="submit" onClick={this._updateBook} className="btn btn-default">Submit</button>
              {success && <p className="bg-success">Success!</p>}
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});