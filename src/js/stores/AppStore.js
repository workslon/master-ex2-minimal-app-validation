var AppDispatcher = require('../dispatchers/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var CHANGE_EVENT = 'change';

var books = [];
var notifications = {
  success: false,
  errors: {}
};

var AppStore = Object.assign({}, EventEmitter.prototype, {
  getBook: function getBook(id) {
    return books.filter(function (book) {
      return book.objectId === id;
    })[0];
  },

  getAllBooks: function () {
    return books;
  },

  getNotifications: function () {
    return notifications;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  switch(action.type) {
    // -- Get all books
    case 'REQUEST_BOOKS_SUCCESS':
      try {
        books = JSON.parse(action.result).results;
        AppStore.emitChange();
      } catch (e) {
        alert('Unvalid remote response format!');
      }
      break;

    // -- Create book
    case 'BOOK_SAVE_SUCCESS':
      try {
        action.data.objectId = JSON.parse(action.result).objectId;
        books.push(action.data);
        notifications.errors = {};
        notifications.success = true;
        AppStore.emitChange();
      } catch (e) {}
      break;

    // --- Update book
    case 'BOOK_UPDATE_SUCCESS':
      books = books.map(function (book) {
        if (book.objectId === action.data.objectId) {
          book.title = action.data.title;
          book.year = action.data.year;
        }
        return book;
      });
      notifications.errors = {};
      notifications.success = true;
      AppStore.emitChange();
      break;

    // -- Destroy book
    case 'BOOK_DESTROY_SUCCESS':
      books = books.filter(function (book) {
        return book.objectId !== action.data.objectId;
      });
      AppStore.emitChange();
      break;

    // --- Validation error
    case 'BOOK_VALIDATION_ERROR':
      notifications.success = false;
      notifications.errors = action.errors;
      AppStore.emitChange();
      break;

    // --- Non-unique ISBN
    case 'NON_UNIQUE_ISBN':
      notifications.success = false;
      notifications.errors = {
        isbn: 'The book with such ISBN already exists!'
      };
      AppStore.emitChange();
      break;

    // --- Clear all notifications (eather errors or success)
    case 'CLEAR_NOTIFICATIONS':
      notifications = {
        success: false,
        errors: {}
      };
      AppStore.emitChange();
      break;
  }
});

module.exports = AppStore;