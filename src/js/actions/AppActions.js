var AppDispatcher = require('../dispatchers/AppDispatcher');
var validator = require('validator');
var XHR = require('../../../node_modules/xhr-parole');
var xhr = new XHR();

xhr.defaults = {
  url: 'https://public-library-api.herokuapp.com/api/classes/Book/',
  headers: {
    'X-Parse-Application-Id': 'MyH7zW1'
  }
};

module.exports = {
  getBooks: function () {
    var promise = xhr.GET();

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOKS',
      success: 'REQUEST_BOOKS_SUCCESS',
      failure: 'REQUEST_BOOKS_ERROR'
    });
  },

  createBook: function (data) {
    validator.validate(data);

    if (validator.isValid()) {
      xhr.GET({
        url: xhr.defaults.url + '?where={"isbn":"' + data.isbn + '"}'
      })
      .then(function (result) {
          try {
            if (JSON.parse(result).results.length) {
              AppDispatcher.dispatch({
                type: 'NON_UNIQUE_ISBN'
              });
            } else {
              AppDispatcher.dispatchAsync(xhr.POST({data: data}), {
                request: 'REQUEST_BOOK_SAVE',
                success: 'BOOK_SAVE_SUCCESS',
                failure: 'BOOK_SAVE_ERROR'
              }, data);
            }
          } catch(e) {}
        },

        function (error) {
          alert('Server error!');
        }
      );
    } else {
      AppDispatcher.dispatch({
        type: 'BOOK_VALIDATION_ERROR',
        errors: validator.errors
      });
    }
  },

  updateBook: function (book, newData) {
    var promise;

    validator.validate(newData);

    if (validator.isValid()) {
      promise = xhr.PUT({
        url: xhr.defaults.url + book.objectId,
        data: newData
      });

      newData.objectId = book.objectId;
      AppDispatcher.dispatchAsync(promise, {
        request: 'REQUEST_BOOK_UPDATE',
        success: 'BOOK_UPDATE_SUCCESS',
        failure: 'BOOK_UPDATE_ERROR'
      }, newData);
    } else {
      AppDispatcher.dispatch({
        type: 'BOOK_VALIDATION_ERROR',
        errors: validator.errors
      });
    }
  },

  deleteBook: function (book) {
    var promise = xhr.DELETE({
      url: xhr.defaults.url + book.objectId
    });

    AppDispatcher.dispatchAsync(promise, {
      request: 'REQUEST_BOOK_DESTROY',
      success: 'BOOK_DESTROY_SUCCESS',
      failure: 'BOOK_DESTROY_ERROR'
    }, book)
  },

  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: 'CLEAR_NOTIFICATIONS'
    });
  }
};